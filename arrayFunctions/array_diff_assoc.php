<?php
$array1 = array("a" => "green", "b" => "brown", "c" => "blue", "red");
$array2 = array("a" => "green",  "red", "yellow");
//$array3 = array("b" => "brown", "a" => "apple", "grape", "mango");
$result = array_diff_assoc($array1, $array2);
echo "<pre>";
print_r($result); //will print the differences of array1 with array2
echo "</pre>";    // red is not printed because they have the same index[0]
?>
