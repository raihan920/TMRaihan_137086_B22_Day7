<?php
/*
 * array_replace — Replaces elements from passed arrays into the first array
 */
$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 4 => "cherry");
$replacements2 = array(0 => "grape");

$basket = array_replace($base, $replacements, $replacements2);
echo "<pre>";
print_r($basket);
echo "</pre>";
?>