<?php
/*
array_unshift — Prepend one or more elements to the beginning of an array
*/

$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
echo "<pre>";
print_r($queue);
echo "</pre>";
?>