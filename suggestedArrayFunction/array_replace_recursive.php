<?php
/*
array_replace_recursive — Replaces elements from passed arrays into the first array recursively
*/
$base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
$replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));

$basket = array_replace_recursive($base, $replacements);
echo "<pre>";
echo "array_replace_recursive:"."\n";
print_r($basket);
echo "</pre>";
$basket = array_replace($base, $replacements);
echo "<pre>";
echo "array_replace:"."\n";
print_r($basket);
echo "</pre>";
?>