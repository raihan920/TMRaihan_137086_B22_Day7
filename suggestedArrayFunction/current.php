<?php
/*
current — Return the current element in an array
 */
$transport = array('foot', 'bike', 'car', 'plane');
$mode = current($transport); // $mode = 'foot';
echo $mode."<br />";
$mode = next($transport);    // $mode = 'bike';
echo $mode."<br />";
$mode = current($transport); // $mode = 'bike';
echo $mode."<br />";
$mode = prev($transport);    // $mode = 'foot';
echo $mode."<br />";
$mode = end($transport);     // $mode = 'plane';
echo $mode."<br />";
$mode = current($transport); // $mode = 'plane';
echo $mode."<br />";

$arr = array();
echo "<pre>";
var_dump(current($arr)); // bool(false)
echo "</pre>";

$arr = array(array());
echo "<pre>";
var_dump(current($arr)); // array(0) { }
echo "</pre>";
?>