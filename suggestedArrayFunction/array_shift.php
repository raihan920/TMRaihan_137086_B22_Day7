<?php
/*
array_shift — Shift an element off the beginning of array
*/
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_shift($stack);
echo "<pre>";
print_r($stack);
echo "</pre>";
?>

