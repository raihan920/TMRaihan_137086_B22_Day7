<?php
/*
array — Create an array
 */

$fruits = array (
    "fruits"  => array("a" => "orange", "b" => "banana", "c" => "apple"),
    "numbers" => array(1, 2, 3, 4, 5, 6),
    "holes"   => array("first", 5 => "second", "third")
);

echo "<pre>";
print_r($fruits);
echo "</pre>";
?>