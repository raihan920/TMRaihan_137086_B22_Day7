<?php
/*
array_values — Return all the values of an array
 */
$array = array("size" => "XL", "color" => "gold");
echo "<pre>";
print_r(array_values($array));
echo "</pre>";
?>