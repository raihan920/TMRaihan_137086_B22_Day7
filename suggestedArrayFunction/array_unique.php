<?php
/*
array_unique — Removes duplicate values from an array
*/

$input = array("a" => "green", "red", "b" => "green", "blue", "red");
$result = array_unique($input);
echo "<pre>";
print_r($result);
echo "</pre>";

$input = array(4, "4", "3", 4, 3, "3");
$result = array_unique($input);
echo "<pre>";
var_dump($result);
echo "</pre>";
?>