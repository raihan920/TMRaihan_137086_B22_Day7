<?php
/*
 *array_push — Push one or more elements onto the end of array
 */
$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry");
echo "<pre>";
print_r($stack);
echo "</pre>";
?>
