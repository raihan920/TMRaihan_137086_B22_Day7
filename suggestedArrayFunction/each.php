<?php
/*
 * each — Return the current key and value pair from an array and advance the array cursor
 */
$foo = array("bob", "fred", "jussi", "jouni", "egon", "marliese");
$bar = each($foo);
echo "<pre>";
print_r($bar);
echo "</pre>";

$foo = array("Robert" => "Bob", "Seppo" => "Sepi");
$bar = each($foo);
echo "<pre>";
print_r($bar);
echo "</pre>";
?>