<?php
/*
 array_pad — Pad array to the specified length with a value
 */

$input = array(12, 10, 9);

$result = array_pad($input, 5, 0);
echo "<pre>";
print_r($result);
echo "</pre>";
// result is array(12, 10, 9, 0, 0)

$result = array_pad($input, -7, -1);
echo "<pre>";
print_r($result);
echo "</pre>";
// result is array(-1, -1, -1, -1, 12, 10, 9)

$result = array_pad($input, 2, "noop");
echo "<pre>";
print_r($result);
echo "</pre>";
// not padded
?>
