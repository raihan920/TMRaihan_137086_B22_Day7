<?php
/*
array_reverse — Return an array with elements in reverse order
*/
$input  = array("php", 4.0, array("green", "red"));
$reversed = array_reverse($input);
$preserved = array_reverse($input, true);

echo "<pre>";
print_r($input);
echo "</pre>";

echo "<pre>";
print_r($reversed);
echo "</pre>";

echo "<pre>";
print_r($preserved);
echo "</pre>";
?>