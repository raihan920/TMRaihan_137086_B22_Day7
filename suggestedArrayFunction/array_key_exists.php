<?php
/*
 * array_key_exists — Checks if the given key or index exists in the array
 */

$search_array = array('first' => 1, 'second' => 4, 'third' => 132, 'fourth' => 334);
if (array_key_exists('first', $search_array)) {
    echo "The 'first' element is in the array";
}
?>