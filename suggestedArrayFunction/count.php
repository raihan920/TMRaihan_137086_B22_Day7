<?php
/*
count — Count all elements in an array, or something in an object
 */
$a[0] = 1;
$a[1] = 3;
$a[2] = 5;
$result = count($a);
echo $result."<br/>";
// $result == 3

$b[0]  = 7;
$b[5]  = 9;
$b[10] = 11;
$result = count($b);
echo $result."<br/>";
// $result == 3

$result = count(null);
echo $result."<br/>";
// $result == 0

$result = count(false);
echo $result;
// $result == 1
?>