<?php
/*
 * array_keys — Return all the keys or a subset of the keys of an array
 */
$array = array(0 => 100, "color" => "red");
echo "<pre>";
print_r(array_keys($array));
echo "</pre>";

$array = array("blue", "red", "green", "blue", "blue");
echo "<pre>";
print_r(array_keys($array, "blue"));
echo "</pre>";

$array = array("color" => array("blue", "red", "green"),
    "size"  => array("small", "medium", "large"));
echo "<pre>";
print_r(array_keys($array));
echo "</pre>";
?>